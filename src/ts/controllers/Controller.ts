import Utils from './Utils';

export class Controller {

    /**
     * Carrega o controller referente a pagina baseada no id da mesma
     */
    protected loader(name:string):void {

        if (name) {
            
            if (document.body.id === name) {

                // import(/* webpackChunkName: "print2" */ controller);

                let params:any = Utils.getParams();

                /**
                 * OnInit
                 */
                this.onInit(params);

                /**
                 * onLoad
                 */
                window.addEventListener('load', () => {
                    
                    this.onLoad(params);
                });

                /**
                 * onReady
                 */
                if (document.readyState !== 'loading') {
            
                    this.onReady(params);
                }
            }
        }
    }

    /**
     * Inicia a aplicação para paginas especificas
     * @param params object
     */
    protected onInit(params?:any) {
        // 
    }
    
    /**
     * Inicia a aplicação quando o document estiver completo
     * @param params object
     */
    protected onReady(params?:any) {
        // 
    }

    /**
     * Inicia quando a pagina estiver carregada por completo
     * @param params object
     */
    protected onLoad(params?:any) {
        // 
    }
}