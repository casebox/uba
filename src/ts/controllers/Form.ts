import Utils from './Utils';

export class Form {

    private id:string
    private form:any;
    private errors:any;
    private done_callback:any;
    private fail_callback:any;
    
    /**
     * Inicializador do validador
     * @param id string
     */
    constructor(form:any) {

        if (typeof form === 'string') {

            this.id = form;
        } else if (form.nodeName === 'FORM') {

            this.form = form
        }

        this.verifyForm();
    }

    /**
     * Quando chmado, esse metodo inicia a validaçao do formulario
     */
    validate() {

        if (this.verifyForm()) {

            if (this.form.hasAttribute('validate') && !this.form.hasAttribute('init')) {

                this.form.setAttribute('init', true);
                this.form.setAttribute('novalidate', true);
                
                this.setAttributes();
        
                // Envio do formulario
                document.addEventListener('submit', this.submit.bind(this), true);
        
                return this;
            }
        }
    }

    /**
     * Callback definido pelo usuario caso o formulario seja validado com sucesso
     * @param callback function
     */
    done(callback:Function) {

        if (typeof callback === 'function') {

            this.done_callback = callback;
        }

        return this;
    }

    /**
     * Callback definido pelo usuario caso o formulario não seja validado corretamente
     * @param callback function
     */
    fail(callback:Function) {

        if (typeof callback === 'function') {

            this.fail_callback = callback;
        }

        return this;
    }

    /**
     * Serialize o formulario instanciado
     */
    serialize() {

        if (this.verifyForm()) {

            let form                = this.form,
                data_object:any     = {},
                data_array:string[] = [],
                
                fields              = null,
                field               = null,
                name                = null,
                value               = null,
        
                is_array            = false,
    
                blockedTypes        = [
                    'button',
                    'submit',
                    'button'
                ],
                allowNodeNames      = [
                    'INPUT', 
                    'TEXTAREA',
                    'SELECT'
                ];
        
            if (form) {
        
                if (form.nodeName === 'FORM') {
    
                    fields  = form.elements;
        
                    for (let i = 0; i < fields.length; i++) {
                        
                        is_array    = false;
                        field       = fields[i];
        
                        if (allowNodeNames.indexOf(field.nodeName) >= 0) {
                            
                            if (blockedTypes.indexOf(field.type) === -1) {
        
                                name = field.name;
        
                                if (name) {

                                    if (!field.checked && ['radio', 'checkbox'].indexOf(field.type) !== -1) {

                                        continue;
                                    }
        
                                    value = field.value;
        
                                    if ((/\[\]$/).test(name)) {
        
                                        is_array = true;
                                    }
        
                                    if (!is_array) {
        
                                        data_object[name] = value;
                                        
                                        data_array.push(name + '=' + value);
                                        
                                    } else {
        
                                        name                = name.replace('[]', '');
                                        data_object[name]   = data_object[name] || [];
        
                                        data_object[name].push(value);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Object.defineProperty(data_object, 'stringfy', {
                get : function () {

                    return function () {

                        return JSON.stringify(data_object);
                    }
                }
            });

            Object.defineProperty(data_object, 'querystring', {
                get : function () {

                    return function () {

                        data_array = data_array.map(function (value:any) {

                            value       = value.split('=');
                            value[1]    = encodeURIComponent(value[1]);
    
                            return value.join('=');
                        });
    
                        return data_array.join('&');
                    }
                }
            });
          
        
            return data_object;
        }

        return false;
    }

    /**
     * Função responsável por receber um objeto com as mensagens customizadas para cada campo, caso haja erro no formulario
     * @param string 
     * OR
     * @param object
     */
    customErrors(errors:any) {

        let fields:any  = this.form.elements,
            field:any   = null,
            name:string;

        if (typeof errors === 'string') {

            errors = JSON.parse(errors);
        }
        
        for (let i = 0; i < fields.length; i++) {

            field   = fields[i];
            name    = field.name;

            if (errors.hasOwnProperty(name)) {

                this.removeErrors(field);

                this.setErrors(field, errors[name]);
            }
        }
    }

    /**
     * Busca e verifica o formulario a ser validado
     */
    private verifyForm() {

        if (this.id) {

            this.form = document.getElementById(this.id);
        }

        if (this.form) {

            if (this.form.nodeName === 'FORM') {

                return true;
            }
        }

        return false;
    }
    
    /**
     * Envio do formulario
     * @param e object
     */
    private submit(e:any) {
    
        e.preventDefault();

        this.validate();

        if (e.target.tagName == 'FORM' && e.target.id === this.form.id) {
        
            if (this.setValidation()) {
                
                if (this.done_callback) {

                    return this.done_callback(this.serialize(), this.form);
                }

                return e.target.submit();
            }

            if (this.fail_callback) {

                return this.fail_callback(this.errors, this.form);
            }
        }
    }
    
    /**
     * Busca e aplica os atributos para validaçao do campo
     */
    private setAttributes() {
    
        let field:any,
            attribute:string[],
            property:string,
            value:string,
            self:any            = this,
            fields:any          = this.form.elements,
            length:number       = fields.length,
            nodeNames:string[]  = [
                'INPUT',
                'TEXTAREA',
                'SELECT'
            ];
    
        for (let i = 0; i < length; i++) {
    
            if (nodeNames.indexOf(fields[i].nodeName) >= 0) {
    
                field = fields[i];
    
                if (field.hasAttribute('validate') && field.getAttribute('validate')) {
    
                    attribute = field.getAttribute('validate').split('|');
    
                    attribute.map(function (node:any) {
                        
                        node = node.split(':');
    
                        property    = node[0];
                        value       = !node[1] ? true : node[1];
                        
                        field.setAttribute(property, value);
    
                        self.setEvents(field);
                    }.bind(this));
    
                    field.removeAttribute('validate');
                }
            }
        }
    }
    
    /**
     * Configura os eventos para o formulario
     */
    private setEvents(field:any) {
    
        let max:string     = field.getAttribute('max'),
            typeOf:string  = field.getAttribute('typeOf');
    
        if (max) {

            field.addEventListener('keypress', Utils.valMax, false);
            field.addEventListener('paste', Utils.valMax, false);
        }
    
        if (['number', 'cpf', 'cnpj', 'phone', 'data'].indexOf(typeOf) >= 0) {
    
            field.addEventListener('keypress', Utils.valNumeric, false);
            field.addEventListener('paste', Utils.valNumeric, false);
        }
    
        if (typeOf === 'string') {
    
            field.addEventListener('keypress', Utils.valString, false);
            field.addEventListener('paste', Utils.valString, false);
        }
    }
    
    /**
     * Valida de o formulario esta pronto para o envio
     */
    private setValidation() {
    
        let field:any,
            typeOf:string,
            required:string,
            max:number,
            min:number,
            value:string,
            length:number,
            fields:any          = this.form.elements,
            disabled:boolean    = false,
            error:boolean       = false,
            submit:boolean      = true,
            errors:any          = [],
            message:any,
            hide_message:any,
            allowNodeNames:any = [
                'INPUT', 
                'TEXTAREA',
                'SELECT'
            ],
            allowTextTypes:any = [
                'text', 
                'email',
                'password',
                'tel',
                'textarea',
                'select-one',
                'file'
            ],
            allowCheckTypes:any = [
                'checkbox', 
                'radio'
            ];
        
        for (let i = 0; i < fields.length; i++) {
    
            field = fields[i];

            if (field) {

                if (allowNodeNames.includes(field.nodeName)) {

                    error       = false;
        
                    value       = field.value;
                    length      = value.length;
            
                    disabled    = field.getAttribute('disabled');
                    typeOf      = field.getAttribute('typeof');
                    required    = field.getAttribute('required');
                    max         = field.getAttribute('max');
                    min         = field.getAttribute('min');

                    if (allowTextTypes.includes(field.type)) {

                        if (!disabled) {
            
                            if (!length && required) {
            
                                if (allowNodeNames.includes(field.nodeName)) {

                                    if (field.nodeName === 'SELECT') {

                                        message = 'O campo não foi selecionado';
                                    } else {

                                        message = 'O campo não pode estar vázio';
                                    }
                                }
            
                                error = true;
                            }

                            if (typeOf && length) {

                                switch(typeOf) {
            
                                    case 'string':
                                        
                                        if (!Utils.valString({ value })) {
            
                                            message = 'O campo não é um texto válido';
            
                                            error = true;
                                        }
            
                                        break;
                                    
                                    case 'number':
                                        
                                        if (!Utils.valNumeric({ value })) {
            
                                            message = 'O campo não é um numérico válido';
            
                                            error = true;
                                        }
            
                                        break;

                                    case 'phone':
                                        if (!Utils.valPhone(value)) {
            
                                            message = 'O campo não é um Telefone válido';
            
                                            error = true;
                                        }
            
                                        break;
                                    
                                    case 'email':

                                        if (!Utils.valEmail(value)) {
            
                                            message = 'O campo não é um e-mail válido!';
            
                                            error = true;
                                        }
            
                                        break;
                                    
                                    case 'cpf':
            
                                        if (!Utils.valCpf(value)) {
            
                                            message = 'O campo não é um CPF válido!';
            
                                            error = true;
                                        }
            
                                        break;

                                    case 'cnpj':
            
                                        if (!Utils.valCnpj(value)) {
            
                                            message = 'O campo não é um CNPJ válido!';
            
                                            error = true;
                                        }
            
                                        break;
            
                                    case 'date':
                                        
                                        if (!Utils.valDate(value)) {
            
                                            message = 'O campo não é uma data válida!';
            
                                            error = true;
                                        }
                                        
                                        break;
                                }
                            }

                            if (max && length) {
                                
                                if (Utils.valMax({ max : max, value : value })) {
            
                                    message = 'O campo atingiu o valor máximo de caracteres';
                                    
                                    error = true;
                                }
                            }

                            if (min && length) {
                                
                                if (Utils.valMin({ min : min, value : value })) {

                                    message = 'O campo não atingiu o valor mínimo de caracteres';
                                    
                                    error = true;
                                }
                            }
                        }
                    }

                    if (allowCheckTypes.includes(field.type)) {

                        if (!disabled) {

                            if (required) {

                                if (!document.querySelector('input[name="' + field.name + '"]:checked')) {

                                    hide_message    = 'O campo não foi marcado corretamente';
                                    message         = '';
                                    error           = true;
                                }
                            }
                        }
                    }

                    if (error) {
            
                        submit = false;

                        if (field.name) {

                            errors[field.name] = {
                                error   : message || hide_message,
                                value   : field.value
                            };
                        }

                        this.setErrors(field, message);
                    } else {

                        this.removeErrors(field);
                    }
                }
            }
        }

        this.errors = errors;

        return submit;
    }

    /**
     * Função responsável por inserir as mensagens dos erros do formulario
     * @param {any} field 
     * @param {string} message 
     */
    private setErrors(field:any, message:string) {

        field.classList.add('field-error');
        field.classList.add('is-invalid'); // Class padrão de erro do Bootstrap 4

        let small:Element,
            last_child  = field.parentNode.lastElementChild;

        if (message) {
            
            if (last_child.nodeName !== 'SMALL') {

                small = document.createElement('small');
            } else {

                small = last_child;
            }

            small.className = 'small-error';
            small.innerHTML = message;

            field.parentNode.insertBefore(small, last_child.nextSibilings);
        }
    }

    /**
     * Função responsável por remover as mensagens de erros do formulario
     * @param {element} field 
     */
    private removeErrors(field:any) { 

        let last_child:any = field.parentNode.lastElementChild;

        if (field.classList.contains('field-error') || field.classList.contains('is-invalid')) {

            field.classList.remove('field-error');
            field.classList.remove('is-invalid');
            field.classList.add('is-valid');

            if (last_child.nodeName === 'SMALL') {
            
                last_child.innerHTML = null;
            }
        }
    }
}
