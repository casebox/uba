export class Utils {

    /**
     * Converte um valor em padrao float ou inteiro para o formato da moeda real
     * @param value Any
     */
    toReal(value:any) {

        let pattern_1:RegExp = /\.?(\d{1,2})$/g,
            pattern_2:RegExp = /(\d)(?=(\d{3})+(?!\d))/g,
            pattern_3:RegExp = /(\d{2})$/g;

        if (!isNaN(value)) {

            if (value.toString().indexOf('.') != -1) {

                value = value.toString();
            } else {

                value = value.toString();
                value = value.replace(pattern_3, '.$1');

            }
        } else if (typeof value === 'string') {

            value = parseFloat(value);
        }
        
        if (value <= 0 || !value) {

            return '0,00';
        }

        value = value.replace(pattern_1, ',$1');
        value = value.replace(pattern_2, '$1.');

        return value;
    };

    /**
     * Converte um valor em padrao texto ou inteiro para o formato decimal
     * @param value Any
     */
    toDecimal(value:any) {

        if (!isNaN(value)) {

            if (value.toString().indexOf('.') != -1) {

                value = value.toString();
            } else {

                value = value.toString();
                value = value.replace(/(\d{2})$/g, '.$1');

            }
        } else if (typeof value === 'string') {

            value = parseFloat(value);
        }

        return parseFloat(value);
    }

    /**
     * Verifica se o valor inserido via evento ou por passagem de parametros é maior ou não
     * @param e Event 
     * OR
     * @param { max : number, value : string }
     */
    valMax(e:any) {

        let target:any      = e.target || e,
            keyCode:number  = target ? (e.keyCode || e.which) : 0,
            max:number      = target.max,
            value:string    = target.value,
            length:number   = 0,

            selectionStart  = 0,
            selectionEnd    = 0;
    
        if (e.type) {

            selectionStart  = target.selectionStart;
            selectionEnd    = target.selectionEnd;

            if (e.type === 'paste') {

                value = e.clipboardData.getData('Text');
            }

            if (selectionStart < selectionEnd && selectionEnd > 0) {

                if (e.type === 'keypress') {

                    value = value.substring(0, selectionStart) + String.fromCharCode(keyCode) + value.substring(selectionEnd, max);
                } else if (e.type === 'paste') {

                    value = value.substring(0, selectionStart) + value + value.substring(selectionEnd, max);
                }
            }
        }

        if (keyCode !== 8) {

            length = value.length;

            if (e.type) {

                if (length >= max) {

                    target.value = value.substring(0, max);

                    return e.preventDefault ? e.preventDefault() : e.returnValue = false;
                }
            } else {

                if (length > max) {

                    return true;
                }

                return false;
            }
        }
    }

    /**
     * Verifica se o valor inserido via parametros é menor ou não
     * @param e { min : number, value : string }
     */
    valMin(e:any) {

        let value:string  = e.value,
            min:number    = e.min,
            length:number = (value ? value.length : 0);

        return length < min;
    }

    /**
     * Verifica se o valor inserido via evento ou por passagem de parametros é um numerico ou nao
     * @param e Event
     * OR
     * @param e { value : string }
     */
    valNumeric(e:any) {

        let target:any      = e.target || e,
            keyCode:number  = target ? (e.keyCode || e.which) : 0,
            value:string    = target.value,
            pattern:RegExp  = /([\d|.|,|\)|\(|\-|\/]+)+/;

        if (e.type === 'paste') {

            value = e.clipboardData.getData('Text');
        } else if (e.type === 'keypress') {
            
            value = String.fromCharCode(keyCode);
        }

        if (keyCode !== 8) {

            if (e.type) {

                if (!pattern.test(value)) {

                    return e.preventDefault ? e.preventDefault() : e.returnValue = false;
                }
            } else {

                if (!pattern.test(value)) {

                    return false;
                }

                return true;
            }
        }
    }

    /**
     * Verifica se o valor inserido via evento ou por passagem de parametros é uma string ou nao
     * @param e Event
     * OR
     * @param e { value : string }
     */
    valString(e:any) {

        let target:any      = e.target || e,
            keyCode:number  = target ? (e.keyCode || e.which) : 0,
            value:string    = target.value,
            pattern:RegExp  = /^[a-zA-Z\s]+$/i;

        if (e.type === 'paste') {

            value = e.clipboardData.getData('Text');
        } else if (e.type === 'keypress') {
            
            value = String.fromCharCode(keyCode);
        }

        if (keyCode !== 8) {

            if (e.type) {

                if (!pattern.test(value)) {

                    return e.preventDefault ? e.preventDefault() : e.returnValue = false;
                }
            } else {

                if (!pattern.test(value)) {

                    return false;
                }

                return true;
            }
        }
    }

    /**
     * Verifica se o email é válido
     * @param email string
     */
    valEmail(email:string) {

        return (/^([\w\-]+(?:\.[\w\-]+)*)@((?:[\w\-]+\.)*\w[\w\-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i).test(email);
    }

    /**
     * Verifica se a data é valida
     * @param date string
     */
    valDate(date:string) {

        return (/^((?:0?[0-9])|(?:[1-2][0-9])|(?:3[01]))\/((?:0?[0-9])|(?:1[0-2]))\/([0-9]{4})?$/i).test(date);
    }

    /**
     * Verifica se o valor inserido é um telefone
     * @param phone string
     */
    valPhone(phone:string) {
        let pattern:any;
        phone  = (phone || null),
        pattern = /^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/;
        //pattern = /(\(?\d{2}\)?\s)?(\d{4,5}\-\d{4})$/;
        
        return pattern.test(phone);
    }

    /**
     * Verifica se o valor inserido é um cnpj
     * @param cnpj string
     */
    valCnpj(cnpj:any) {
        
        let size:any,
            digits:any,
            sum:any,
            i:any,
            numbers:any,
            result:any,
            pos:any;

        cnpj = cnpj.replace(/[^\d]+/g,'');
     
        if(cnpj == '') return false;
         
        if (cnpj.length != 14)
            return false;
     
        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" || 
            cnpj == "11111111111111" || 
            cnpj == "22222222222222" || 
            cnpj == "33333333333333" || 
            cnpj == "44444444444444" || 
            cnpj == "55555555555555" || 
            cnpj == "66666666666666" || 
            cnpj == "77777777777777" || 
            cnpj == "88888888888888" || 
            cnpj == "99999999999999")
            return false;
             
        // Valida DVs
        size = cnpj.length - 2
        numbers = cnpj.substring(0,size);
        digits = cnpj.substring(size);
        sum = 0;
        pos = size - 7;
        for (i = size; i >= 1; i--) {
            sum += numbers.charAt(size - i) * pos--;
            if (pos < 2) {
                pos = 9;
            }
        }
        result = sum % 11 < 2 ? 0 : 11 - sum % 11;
        if (result != digits.charAt(0)) {
            return false;
        }
             
        size = size + 1;
        numbers = cnpj.substring(0,size);
        sum = 0;
        pos = size - 7;
        for (i = size; i >= 1; i--) {
            sum += numbers.charAt(size - i) * pos--;
            if (pos < 2) {
                    pos = 9;
            }
        }
        result = sum % 11 < 2 ? 0 : 11 - sum % 11;
        if (result != digits.charAt(1)) {
              return false;
        }
               
        return true;
        
    }

    /**
     * Verifica se o valor inserido é um cpf
     * @param cpf string
     */
    valCpf(cpf:any) {

        let numbers:any,
            digits:any,
            sum:any,
            i:any,
            result:any,
            digits_equals:any = 1;
        
        cpf = (cpf || null).replace(/[.|\-]/g, '');

        if (cpf.length < 11) {

            return false;
        }

        for (i = 0; i < cpf.length - 1; i = i + 1) {

            if (cpf.charAt(i) !== cpf.charAt(i + 1)) {

                digits_equals = 0;

                break;
            }
        }

        if (!digits_equals) {

            numbers = cpf.substring(0, 9);
            digits  = cpf.substring(9);
            sum     = 0;

            for (i = 10; i > 1; i = i - 1) {

                sum += numbers.charAt(10 - i) * i;
            }

            result = sum % 11 < 2 ? 0 : 11 - sum % 11;

            if (parseInt(result, 10) !== parseInt(digits.charAt(0), 10)) {

                return false;
            }

            numbers = cpf.substring(0, 10);
            sum = 0;

            for (i = 11; i > 1; i = i - 1) {
                sum += numbers.charAt(11 - i) * i;
            }

            result = sum % 11 < 2 ? 0 : 11 - sum % 11;

            if (parseInt(result, 10) !== parseInt(digits.charAt(1), 10)) {

                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Retorna um objeto com os parametros da url
     */
    getParams(querystring?:string) {

        querystring = querystring || '';
        querystring = querystring.split('?')[1];

        let params:string   = querystring || window.location.search.substring(1),
            arr:string[]    = params.split('&'),
            values:any      = null,
            response:any    = {};
    
        if (params.length) {

            for (let i = 0; i < arr.length; i++) {

                values = arr[i].split('=');

                response[values[0]] = values[1];
            }
            
            Object.defineProperty(response, 'stringfy', {
                get : function () {
    
                    return function () {

                        return JSON.stringify(response);
                    }
                }
            });
    
            Object.defineProperty(response, 'querystring', {
                get : function () {
    
                    return function () {

                        return params;
                    }
                }
            });
        }

        return response;
    };

    /**
     * Limpa a string de acentos
     */
    clearAccents(str:string) {

        let accents:string      = 'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž',
            accentsOut:string   = "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz",
            i:number            = 0,
            x:any               = null,
            str_len:number      = 0,
            str_arr:string[]    = [];

        str_arr = str.split('');
        str_len = str.length;

        for (let i = 0; i < str_len; i++) {
            
            if ((x = accents.indexOf(str[i])) != -1) {
                
                str_arr[i] = accentsOut[x];
            }
        }

        return str_arr.join('');
    };

    /**
     * Converte milesegundos para minutos
     */
    millisecondToMinute(millisecond:any) {

        return (parseFloat(millisecond) || 0) / 60000;
    };

    /**
     * Converte minutos para milesegundos
     */
    minuteToMillisecond(minute:any) {

        return (parseFloat(minute) || 0) * 60000;
    };

    /**
     * Converte objeto para array
     */
    objectToArray(object:any) {
        
        return Object.keys(object).map(function (index:any) {

            return object[index];
        });
    };

    /**
     * Função para obter o dominio atual do site
     */
    getDomain() {

        let origin:string   = window.location.origin,
            protocol:string = window.location.protocol,
            hostname:string = window.location.hostname;

        origin = origin.replace(protocol, '');
        origin = origin.replace('//', '');
        origin = origin.replace('www.', '');

        return (hostname === 'localhost') ? 'localhost' : origin;
    };

    /**
     * Função para setar cookies
    */
    setCookie(config:any) {
        if (config) {

            if (config.hasOwnProperty('name') && config.hasOwnProperty('value')) {

                let today:any = new Date(),
                    expire:any = new Date(),
                    days:any = (config.days || 1),
                    name:string = config.name,
                    value: any = config.value,
                    domain:string = config.domain || this.getDomain(),
                    path:string = config.path || '/';

                expire.setTime(today.getTime() + 3600000 * 24 * days);

                document.cookie = name + '=' + value + ';expires=' + expire.toGMTString() + ';domain=' + domain + ';path=' + path;

                return true;
            }
        }

        return false;
    };

    /**
     * Função para obter o valor de um cookie
     */
    getCookie(name:string) {

        let query:any = document.cookie.split(';'),
            match:any = null,
            result:any = [];

        for (var index in query) {

            if (typeof query[index] === 'string') {

                match = query[index].trim().split('=');

                if (name) {

                    if (name === match[0]) {

                        result = (match.length > 2) ? match[1] + '=' + match[2] : match[1];

                        result = result === 'true' ? true : result;
                        result = result === 'false' ? false : result;

                        break;
                    } else {

                        result = false;
                    }
                } else {

                    result[match[0]] = match[1];
                }
            }
        }

        return result;
    };

    /**
     * Função para apagar um cookie
     */
    deleteCookie(name:string) {

        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';

        return false;
    };
    
    /**
     * Função para identificar se o navegador é de um dispositivo mobile
     */
    isMobile() {
        
        let devices:any = [
                /Android/i,             // Android
                /BlackBerry/i,          // BlackBerry
                /iPhone|iPad|iPod/i,    // IOS
                /Opera Mini/i,          // Opera
                /IEMobile/i,            // IE
                /WPDesktop/i            // IE
            ],
            userAgent:string    = navigator.userAgent,
            result:boolean      = false;

        devices.forEach(function (item:any) {

            if (item.test(userAgent)) {

                result = true;
            }
        });

        return result;
    }

    /**
     * Recebe arrays como argumentos para comparação
     */
    compareArray (...args: any[]) {
        
        let res:boolean = false;

        for (let i = 0; i < args.length; i++) {

            const current:Array<any>    = args[i];
            const next:Array<any>       = args[i + 1];

            if (next) {

                if (!Array.isArray(current) || !Array.isArray(next) || current.length !== next.length) {

                    res = false;
                    
                    break;
                }

                res = current.every((x) => {

                    return next.includes(x);
                });
            }
        }

        return res;
    }
}

export default new Utils;